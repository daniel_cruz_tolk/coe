<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {

        User::find(1)->assignRole('Super admin');
        User::find(2)->assignRole('Default');

    }
}