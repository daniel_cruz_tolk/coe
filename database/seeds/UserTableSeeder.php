<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {

        // Add the master administrator, user id of 1
        User::create([
            'name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret')
        ]);

        User::create([
            'name' => 'Default',
            'last_name' => 'User',
            'email' => 'user@user.com',
            'password' => bcrypt('secret')
        ]);

    }
}