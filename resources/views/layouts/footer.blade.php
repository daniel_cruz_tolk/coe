<footer class="main-footer">
    <strong>Copyright © <a href="http://tolk.mx">TOLK</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.3
    </div>
</footer>